const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

const db = admin.firestore();

exports.setupTesting = functions.https.onRequest(async (request, response) => {
  const promises = []
  const querySnapshot = await db.collection('posts').get()
  querySnapshot.forEach(documentSnapshot => {
    promises.push(documentSnapshot.ref.delete())
  });
  await Promise.all(promises)
  await db.doc('posts/test-post').set({
    text: 'test post body',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
  })
  return response.send("ok");
});
