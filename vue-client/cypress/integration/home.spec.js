const utils = require('../support/utils')

describe('The Home Page', () => {
  // it('successfully loads', () => {
  //   cy.visit('/')
  //   cy.url().should('include', '/sign-in')
  // })

  it('successfully signs in', () => {
    cy.login('viewer@litslink.com', '123123')
    cy.contains('Posts', { timeout: 10000 })
    cy.get('#new-post-text').type(`hi from test ${utils.MARK}`)
    cy.get('#create-post-btn').click()
    cy.contains('.post-card', `hi from test ${utils.MARK}`)
  })
})
