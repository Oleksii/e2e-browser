const guestRoutes = [
  'sign-in',
]

export default ({ store, redirect, route: { name } }) => {
  if (!store.getters['auth/isSignedIn'] && !guestRoutes.includes(name)) {
    return redirect('/sign-in')
  }
  if (store.getters['auth/isSignedIn'] && guestRoutes.includes(name)) {
    return redirect('/')
  }
}
