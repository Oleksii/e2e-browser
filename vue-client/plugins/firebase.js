import { initializeApp } from "firebase/app"

const firebaseConfig = {
  apiKey: "AIzaSyCA_1smH9dD4pTrvZya-rH4kHc93m5XXXc",
  authDomain: "e2e-browser.firebaseapp.com",
  projectId: "e2e-browser",
  storageBucket: "e2e-browser.appspot.com",
  messagingSenderId: "407867304469",
  appId: "1:407867304469:web:8010331018b45edb5a7c1c",
  measurementId: "G-GG9X1JZ4J1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app
