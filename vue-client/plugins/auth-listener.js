import { getAuth, onAuthStateChanged } from "firebase/auth";

export default ({ store, redirect, route: { name } }) => {
  const auth = getAuth()

  onAuthStateChanged(auth, user => {
    store.commit('auth/setUser', user)
    if (!user && name !== 'sign-in') {
      redirect('/sign-in')
    }
  })

  return new Promise(resolve => {
    const initialListener = onAuthStateChanged(auth, user => {
      initialListener()
      resolve(user)
    })
  })
}
