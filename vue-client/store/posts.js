import {
  getFirestore,
  collection,
  onSnapshot,
  query,
  serverTimestamp,
  addDoc
} from "firebase/firestore";
import Vue from 'vue'

export const state = () => ({
  data: {},
  unsub: null,
  isCreating: false,
})

export const mutations = {
  addData(state, { id, data }) {
    Vue.set(state.data, id, data)
  },

  setUnsub(state, unsub) {
    state.unsub = unsub
  },

  setIsCreating(state, value) {
    state.isCreating = value
  },
}

export const actions = {
  fetchPosts({ state, commit }) {
    if (state.unsub) {
      return
    }
    const db = getFirestore()
    const postsRef = collection(db, 'posts')
    const postsQuery = query(postsRef)
    const unsub = onSnapshot(postsQuery, querySnapshot => {
      querySnapshot.forEach((doc) => {
        commit('addData', {
          id: doc.id,
          data: doc.data(),
        })
      });
    })
    commit('setUnsub', unsub)
  },

  async createPost({ commit }, params) {
    const db = getFirestore()
    const postsRef = collection(db, 'posts')
    commit('setIsCreating', true)
    try {
      await addDoc(postsRef, {
        ...params,
        createdAt: serverTimestamp(),
      })
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log('error during post creation', e)
      throw e
    } finally {
      commit('setIsCreating', false)
    }
    
  },

  unsubscribe({ state }) {
    state.unsub()
  },
}
