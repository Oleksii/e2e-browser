import { getAuth, signInWithEmailAndPassword, signOut } from 'firebase/auth'

export const state = () => ({
  isLoading: false,
  error: null,
  user: null,
})

export const mutations = {
  setError(state, error) {
    state.error = error
  },
  setIsLoading(state, value) {
    state.isLoading = value
  },
  setUser(state, userData) {
    state.user = userData
      ? userData.toJSON()
      : null
  }
}

export const actions = {
  async signIn({ commit }, params) {
    commit('setIsLoading', true)
    try {
      const auth = getAuth()
      await signInWithEmailAndPassword(auth, params.email, params.password)
    } catch (e) {
      commit('setError', e.message || 'Unknown sign in error')
      throw e
    } finally {
      commit('setIsLoading', false)
    }
  },

  signOut() {
    const auth = getAuth()
    signOut(auth)
  },
}

export const getters = {
  isSignedIn: state => !!state.user,
}
